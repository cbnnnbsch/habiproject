from django.shortcuts import render
from django.http import HttpResponseBadRequest
from .models import BudgetRequest, APIUser, Category

from django.http import *


def new_budget_request(request):
    if request.method == "GET":
        # required params
        description, state, email, phone, address = None, None, None, None, None
        # optional params
        title, category = None, None
        error_msg = ""

        # retrieve required params
        description = request.GET.get('description')
        state = int(request.GET.get('state')) if request.GET.get('state') else None
        email = request.GET.get('email')
        phone = request.GET.get('phone')
        address = request.GET.get('address')

        # validate
        if not description:
            error_msg += "<br>description param is missing"
        if not state:
            error_msg += "<br>state param is missing"
        if not email:
            error_msg += "<br>email param is missing"
        if not phone:
            error_msg += "<br>phone param is missing"
        if not address:
            error_msg += "<br>address param is missing"

        if error_msg != "":
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1>" + error_msg, content_type="text/html")

        try:
            user = APIUser.objects.get(email=email)
            save_user = False
            if user.address is not address:
                user.address = address
                save_user = True
            if user.phone is not phone:
                user.phone = phone
                save_user = True
        except APIUser.DoesNotExist:
            user = APIUser(email=email, address=address, phone=phone)
            save_user = True
        except APIUser.MultipleObjectsReturned:
            return HttpResponseServerError(content="<h1>Internal Error (500)</h1> Multiple users with email:"
                                                   + email, content_type="text/html")
        if save_user:
            user.save()

        budget = BudgetRequest(description=description, state=state, user=user)

        # retrieve optional params
        budget.title = request.GET.get('title')
        try:
            budget.category = Category.objects.get(id=(request.GET.get('category')))
        except Category.DoesNotExist:
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Category doesn't exist: "
                                                  + request.GET.get('category'),
                                          content_type="text/html")
        budget.save()

        return JsonResponse({"status": "success", "message": "Data added", "data": budget.to_json()})
    else:
        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Method unexpected. Method expected: GET ",
                                      content_type="text/html")


def modify_budget_request(request):
    if request.method == "GET":
        # required params
        id_budget, title, description, category = None, None, None, None
        error_msg = ""
        # retrieve required params
        id_budget = request.GET.get('id_budget')
        title = request.GET.get('title')
        description = request.GET.get('description')
        category = request.GET.get('category')

        # validate
        if not id_budget:
            error_msg = "<br>The identifier is missing!!"

        if error_msg != "":
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1>" + error_msg, content_type="text/html")

        try:
            budget = BudgetRequest.objects.get(id=id_budget)
            if budget.state == '0':
                save_budget = False
                if budget.description is not description:
                    budget.description = description
                    save_budget = True
                if budget.title is not title:
                    budget.phone = title
                    save_budget = True
                if budget.category and budget.category.id is not category:
                    try:
                        budget.category = Category.objects.get(id=(request.GET.get('category')))
                        save_budget = True
                    except Category.DoesNotExist:
                        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Category doesn't exist: "
                                                              + request.GET.get('category'),
                                                      content_type="text/html")
            else:
                return HttpResponseForbidden(content="<h1>Forbidden modification (403)</h1> The budget can't be "
                                                     "modified because its state is: " + budget.get_state_display(),
                                             content_type="text/html")
        except BudgetRequest.DoesNotExist:
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> The budget identifier does not exist: "
                                                  + id_budget, content_type="text/html")
        except BudgetRequest.MultipleObjectsReturned:
            return HttpResponseServerError(content="<h1>Internal Error (500)</h1> Multiple budgets returned for the id:"
                                                   + id_budget, content_type="text/html")
        if save_budget:
            budget.save()
            success_msg = "Budget updated"
        else:
            success_msg = "Budget already updated"

        return JsonResponse({"status": "success", "message": success_msg, "data": budget.to_json()})
    else:
        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Method unexpected. Method expected: GET ",
                                      content_type="text/html")


def publish_budget_request(request):
    if request.method == "GET":
        # required params
        id_budget = None
        error_msg = ""

        # retrieve required params
        id_budget = request.GET.get('id_budget')

        # validate
        if not id_budget:
            error_msg = "<br>The identifier is missing!!"

        if error_msg != "":
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1>" + error_msg, content_type="text/html")

        try:
            budget = BudgetRequest.objects.get(id=id_budget)
            if budget.state != '0':
                error_msg += "<br> The state of the budget is not on pending status: " + budget.get_state_display()
            if budget.description is (None or ""):
                error_msg += '<br> The budget has no description'
            if budget.category is None:
                error_msg += '<br> The budget has no category'

            if error_msg == "":
                budget.state = '1'
                budget.save()
                return JsonResponse({"status": "success", "message": "The budget was updated",
                                     "data": budget.to_json()})
            else:
                return HttpResponseForbidden(content="<h1>Forbidden modification (403)</h1>" + error_msg,
                                             content_type="text/html")
        except BudgetRequest.DoesNotExist:
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> The budget identifier does not exist: "
                                                  + id_budget, content_type="text/html")
        except BudgetRequest.MultipleObjectsReturned:
            return HttpResponseServerError(content="<h1>Internal Error (500)</h1> Multiple budgets returned for the id:"
                                                   + id_budget, content_type="text/html")
    else:
        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Method unexpected. Method expected: GET ",
                                      content_type="text/html")


def discard_budget_request(request):
    if request.method == "GET":
        # required params
        id_budget = None
        error_msg = ""

        # retrieve required params
        id_budget = request.GET.get('id_budget')

        # validate
        if not id_budget:
            error_msg = "<br>The identifier is missing!!"

        if error_msg != "":
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1>" + error_msg, content_type="text/html")

        try:
            budget = BudgetRequest.objects.get(id=id_budget)
            if budget.state == '2':
                error_msg += "<br> The budget can't be discarded, it is already in 'discard' state"
                return HttpResponseForbidden(content="<h1>Forbidden modification (403)</h1>" + error_msg,
                                             content_type="text/html")
            else:
                budget.state = '2'
                budget.save()
                return JsonResponse({"status": "success", "message": "The budget was discarded",
                                     "data": budget.to_json()})
        except BudgetRequest.DoesNotExist:
            return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> The budget identifier does not exist: "
                                                  + id_budget, content_type="text/html")
        except BudgetRequest.MultipleObjectsReturned:
            return HttpResponseServerError(content="<h1>Internal Error (500)</h1> Multiple budgets returned for the id:"
                                                   + id_budget, content_type="text/html")
    else:
        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Method unexpected. Method expected: GET ",
                                      content_type="text/html")


def list_budgets_request(request):
    if request.method == "GET":
        # required params
        email = None

        # retrieve required params
        email = request.GET.get('email')

        # if its a paginated list, we ask for page start idx and size of page
        start = int(request.GET.get('start')) if request.GET.get('start') else 0
        page_size = int(request.GET.get('page_size')) if request.GET.get('page_size') else 25
        amount_page = start * page_size

        # validate
        if email:
            list_budgets = BudgetRequest.objects.filter(user__email=email)
        else:
            list_budgets = BudgetRequest.objects.all()

        total_items = list_budgets.count()
        output_list = [x.to_json() for x in list_budgets[amount_page:amount_page + page_size]]

        if total_items == 0:
            msg_success = "No budgets were found"
        else:
            msg_success = "Success"

        return JsonResponse({"status": "success",
                             "message": msg_success,
                             "response": {
                                 "data": output_list,
                                 "total_items": total_items,
                                 "start": start,
                                 "page_size": page_size
                             }})

    else:
        return HttpResponseBadRequest(content="<h1>Bad Request (400)</h1> Method unexpected. Method expected: GET ",
                                      content_type="text/html")
