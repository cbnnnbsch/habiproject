from django.db import models

# Create your models here.
ESTIMATED_DATE_OPTIONS = [
    ('0', 'Lo antes posible'),
    ('1', 'De 1 a 3 meses'),
    ('2', 'Más de 3 meses')
]

PRICE_PREFERENCE_OPTIONS = [
    ('0', 'Lo más barato'),
    ('1', 'Relación calidad precio'),
    ('2', 'Mejor calidad')
]

STATE_OPTIONS = [
    ('0', 'Pendiente'),
    ('1', 'Publicada'),
    ('2', 'Descartada')
]


class Category(models.Model):
    name = models.CharField(blank=False, max_length=50)

    def __unicode__(self):
        return self.name

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name
        }


class SubCategory(models.Model):
    parent_category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(blank=False, max_length=50)


class APIUser(models.Model):
    address = models.CharField(blank=False, max_length=100)
    email = models.CharField(blank=False, max_length=50)
    phone = models.CharField(blank=False, max_length=10)

    def to_json(self):
        return {
            'id': self.id,
            'email': self.email,
            'address': self.address,
            'phone': self.phone
        }

class BudgetRequest(models.Model):
    title = models.CharField(blank=False, max_length=350)
    description = models.CharField(blank=False, max_length=350)
    state = models.CharField(blank=False, max_length=1, choices=STATE_OPTIONS)
    #estimated_date = models.CharField(max_length=1, choices=ESTIMATED_DATE_OPTIONS)
    #price_preference = models.CharField(max_length=1, choices=PRICE_PREFERENCE_OPTIONS)
    category = models.ForeignKey(Category, blank=False, on_delete=models.CASCADE)
    #sub_category = models.ForeignKey(SubCategory, blank=False, on_delete=models.CASCADE)
    user = models.ForeignKey(APIUser, on_delete=models.CASCADE)

    def to_json(self):
        return {
            'id': self.id,
            'title': self.description,
            'description': self.description,
            'category': self.category.to_json(),
            'user': self.user.to_json()
        }


