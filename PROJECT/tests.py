from django.test import TestCase
from PROJECT.models import *
from django.test import Client


class APINewRequestView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_when_invalid_params(self):
        response = self.client.get("/budget_request/", [])
        self.assertEqual(response.status_code, 400)

    def test_when_valid_params(self):
        Category(name="CATEGORY 1", id=1).save()
        response = self.client.get("/budget_request/", {'description': 'description',
                                                        'state': '1',
                                                        'email': 'example@example.com',
                                                        'phone': '999999999',
                                                        'address': 'address, 10',
                                                        'title': 'title title',
                                                        'category': '1'})
        self.assertEqual(response.status_code, 200)

    def test_when_multiple_users(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        APIUser(email='example@example.com', phone='888888888', address='address, 11').save()
        Category(name="CATEGORY 1", id=1).save()

        response = self.client.get("/budget_request/", {'description': 'description',
                                                        'state': '1',
                                                        'email': 'example@example.com',
                                                        'phone': '999999999',
                                                        'address': 'address, 10',
                                                        'title': 'title title',
                                                        'category': '1'})
        self.assertEqual(response.status_code, 500)
        self.assertIn("Multiple users with email:", str(response.content))

    def test_when_missing_email(self):
        response = self.client.get("/budget_request/", {'description': 'description',
                                                        'state': '1',
                                                        'phone': '999999999',
                                                        'address': 'address, 10',
                                                        'title': 'title title',
                                                        'category': '1'})
        self.assertEqual(response.status_code, 400)
        self.assertIn("email", str(response.content))


class APIModifyRequestView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_when_invalid_params(self):
        response = self.client.get("/modify_budget/", [])
        self.assertEqual(response.status_code, 400)

    def test_when_valid_params(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 1", id=1).save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='0', title="Title", id=1).save()
        response = self.client.get("/modify_budget/", {'id_budget': '1',
                                                       'description': 'desc B',
                                                       'category': '1'})
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.json()['data']['category']['name'], 'CATEGORY 2')
        self.assertNotEqual(response.json()['data']['description'], 'desc A')

    def test_when_invalid_state(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 1", id=1).save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/modify_budget/", {'id_budget': '1',
                                                       'description': 'desc B',
                                                       'category': '1'})
        self.assertEqual(response.status_code, 403)
        self.assertIn("modified because its state is", str(response.content))

    def test_when_invalid_identifier(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 1", id=1).save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/modify_budget/", {'id_budget': '2',
                                                       'description': 'desc B',
                                                       'category': '1'})
        self.assertEqual(response.status_code, 400)
        self.assertIn("The budget identifier does not exist", str(response.content))


class APIPublishRequestView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_when_invalid_params(self):
        response = self.client.get("/publish_budget/", [])
        self.assertEqual(response.status_code, 400)
        self.assertIn("The identifier is missing!!", str(response.content))

    def test_when_valid_params(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='0', title="Title", id=1).save()
        response = self.client.get("/publish_budget/", {'id_budget': '1'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['message'], 'The budget was updated')

    def test_when_invalid_state(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/publish_budget/", {'id_budget': '1'})
        self.assertEqual(response.status_code, 403)
        self.assertIn("Forbidden modification", str(response.content))

    def test_when_invalid_identifier(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/publish_budget/", {'id_budget': '2'})
        self.assertEqual(response.status_code, 400)
        self.assertIn("The budget identifier does not exist", str(response.content))


class APIDiscardRequestView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_when_invalid_params(self):
        response = self.client.get("/discard_request/", [])
        self.assertEqual(response.status_code, 400)
        self.assertIn("The identifier is missing!!", str(response.content))

    def test_when_valid_params(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/discard_request/", {'id_budget': '1'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['message'], 'The budget was discarded')

    def test_when_invalid_state(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='2', title="Title", id=1).save()
        response = self.client.get("/discard_request/", {'id_budget': '1'})
        self.assertEqual(response.status_code, 403)
        self.assertIn("The budget can't be discarded, it is already in 'discard' state", str(response.content))

    def test_when_invalid_identifier(self):
        APIUser(email='example@example.com', phone='999999999', address='address, 10').save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='1', title="Title", id=1).save()
        response = self.client.get("/discard_request/", {'id_budget': '2'})
        self.assertEqual(response.status_code, 400)
        self.assertIn("The budget identifier does not exist", str(response.content))

class APIListRequestView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_when_no_params(self):
        response = self.client.get("/list_budgets/", [])
        self.assertEqual(response.status_code, 200)


    def test_when_valid_email(self):
        a = APIUser(email='example@example.com', phone='999999999', address='address, 10', id=1).save()
        b = APIUser(email='example1@example.com', phone='999999999', address='address, 10', id=2).save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='2', title="Title", id=1).save()
        BudgetRequest(description="desc B", category_id=2, user_id=2, state='1', title="Title", id=2).save()
        BudgetRequest(description="desc C", category_id=2, user_id=2, state='2', title="Title", id=3).save()
        response = self.client.get("/list_budgets/", {'email': 'example@example.com'})
        self.assertEqual(response.status_code, 200)
        self.assertEquals(1, int(response.json()['response']['total_items']))

    def test_when_invalid_identifier(self):
        a = APIUser(email='example@example.com', phone='999999999', address='address, 10', id=1).save()
        b = APIUser(email='example1@example.com', phone='999999999', address='address, 10', id=2).save()
        Category(name="CATEGORY 2", id=2).save()
        BudgetRequest(description="desc A", category_id=2, user_id=1, state='2', title="Title", id=1).save()
        BudgetRequest(description="desc B", category_id=2, user_id=2, state='1', title="Title", id=2).save()
        BudgetRequest(description="desc C", category_id=2, user_id=2, state='2', title="Title", id=3).save()
        BudgetRequest(description="desc D", category_id=2, user_id=1, state='1', title="Title", id=4).save()
        response = self.client.get("/list_budgets/", {'id_budget': '2', 'start': 2})
        self.assertEqual(response.status_code, 200)
        self.assertEquals(4, int(response.json()['response']['total_items']))



